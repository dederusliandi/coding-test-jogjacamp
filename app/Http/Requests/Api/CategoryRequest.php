<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:191',
            'is_publish' => 'required'
        ];
    }

    protected function failedValidation(Validator $validator) { 
        throw new HttpResponseException(
            response()->json([
                'code' => 422,
                'message' => 'all field is required, check your input!',
                'data' => $validator->messages()->all()
            ], 422)
        ); 
    }
}
