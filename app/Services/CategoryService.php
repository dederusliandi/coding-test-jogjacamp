<?php

namespace App\Services;

use App\Repositories\Contracts\CategoryInterface as CategoryRepo;
use App\Services\Contracts\CategoryInterface;
use App\Models\Category;
use DB;
use App\Jobs\CategoryCreatedMailJob;
use App\Jobs\CategoryDeletedMailJob;

class CategoryService implements CategoryInterface
{
    protected $categoryRepo;

    public function __construct(CategoryRepo $categoryRepo)
    {
        $this->categoryRepo = $categoryRepo;
    }

    public function getAllSimplePaginatedWithParams($params, $limit = 10)
    {
       return $this->categoryRepo->getAllSimplePaginatedWithParams($params, $limit);
    }

    public function getAllPaginatedWithParams($params, $limit = 10)
    {
       return $this->categoryRepo->getAllPaginatedWithParams($params, $limit);
    }

     /**
     * Find data by id
     *
     * @param $id
     * @param array $columns
     *
     * @return mixed
     */
    public function find($id)
    {
        return $this->categoryRepo->find($id);
    }

    /**
     * @param $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function create($request)
    {
        $permissions = DB::transaction(function () use ($request) {
            $input = $request->all();
            $created = $this->categoryRepo->create($input);
            dispatch(new CategoryCreatedMailJob($created));

            return $created;
        });

        return $permissions;
    }

     /**
     * @param $id
     * @param $request
     *
     * @return mixed
     * @throws \Throwable
    */
    public function update($request, $id)
    {
        $permissions = DB::transaction(function () use ($request, $id) {
            $input = $request->except('_token','_method');
            return $this->categoryRepo->update($input, $id);
        });

        return $permissions;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function delete($id)
    {
        dispatch(new CategoryDeletedMailJob());

        return $this->categoryRepo->delete($id);
    }
}
