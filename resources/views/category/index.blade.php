@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <div class="card border-0 shadow rounded">
                <div class="card-body">
                    @if(Session::has('failed'))
                        <p class="alert alert-danger">{{ Session::get('failed') }}</p>
                    @endif

                    @if(Session::has('success'))
                        <p class="alert alert-success">{{ Session::get('success') }}</p>
                    @endif

                    <a href="{{ route('categories.create') }}" class="btn btn-md btn-success mb-3">Add New</a>

                    <form class="form-inline float-right" method="GET" action="{{ route('categories.index') }}">
                        <div class="form-group">
                            <div class="input-group">
                                <input class="form-control" id="q" placeholder="" name="search" type="text" value="{{ request()->search }}" />
                                <button class="btn btn-primary btn-flat input-group-addon" type="submit">Search</button>
                            </div>
                        </div>
                    </form>
                    <br>
                    <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th scope="col">No</th>
                            <th scope="col">Name</th>
                            <th scope="col">Is Publish</th>
                            <th scope="col">Created at</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                            @forelse ($categories as $key => $category)
                                <tr>
                                    <td class="text-center">
                                        {{ $categories->firstItem() + $key }}
                                    </td>
                                    <td>{{ $category->name }}</td>
                                    <td>{{ $category->is_publish == true ? 'Pusblihed' : 'Not Published' }}</td>
                                    <td>{{ $category->created_at }}</td>
                                    <td class="text-center">

                                        <form onsubmit="return confirm('Are you sure you want to delete data?');" action="{{ route('categories.destroy', $category->id) }}" method="POST">    
                                            <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-sm btn-primary">Edit</a>  
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <div class="alert alert-danger">
                                    Data Category Not Found.
                                </div>
                            @endforelse
                        </tbody>
                    </table>  
                    <div class="float-right">
                        {{ $categories->appends(['search' => Request::get('search')])->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection